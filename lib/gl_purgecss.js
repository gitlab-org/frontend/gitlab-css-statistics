const { PurgeCSS } = require('purgecss');
const { purgeHtml } = require('purge-from-html');
const { mkdirSync, existsSync, writeFileSync } = require('fs');

const config = require('../crawler_config');
const cssMap = require('../tmp/css-map.json');

if (!existsSync('tmp/report')) {
  mkdirSync('tmp/report');
}

(async () => {
  const purgeCSSResult = await new PurgeCSS().purge({
    content: [
      'tmp/crawled_pages/*.html',
      '../gdk/gitlab/app/**/*.vue',
      ,
      '../gdk/gitlab/app/**/*.js',
      ,
      '../gdk/gitlab/app/views/**/*.html.haml',
      '../gdk/gitlab/app/views/**/*.erb',
      '../gdk/gitlab/app/views/**/*.rb',
    ],
    css: ['static/application-local.css'],
    extractors: [
      {
        extractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
        extensions: ['vue', 'js'],
      },
      {
        extractor: (content) => content.match(/[A-Za-z0-9-_:/]+/g) || [],
        extensions: ['haml', 'erb', 'rb'],
      },
    ],
    fontFace: true,
    variables: true,
    rejected: true,
  });
  console.log('Purge Result : ', purgeCSSResult);

  if (purgeCSSResult.length > 0) {
    const mainResult = purgeCSSResult[0];
    writeFileSync('tmp/new_application.css', mainResult.css);
    writeFileSync(
      'tmp/purgeResultRejected.json',
      JSON.stringify(mainResult.rejected, null, 2)
    );

    mainResult.rejected.forEach((rejectedSelector) => {
      if (cssMap.nodes[rejectedSelector]) {
        cssMap.nodes[rejectedSelector].rejectedPurge = true;
      }
    });

    writeFileSync('tmp/css-map.json', JSON.stringify(cssMap, null, 2));

    let outStr = `<h4>${mainResult.rejected.length} Selectors too much</h4>`;
    for (let i = 0; i < mainResult.rejected.length; i++) {
      outStr += mainResult.rejected[i] + '<br/>';
    }
    writeFileSync('tmp/report/index.html', outStr);
  }
})();
