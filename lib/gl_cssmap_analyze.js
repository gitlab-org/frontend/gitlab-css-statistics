const { mkdirSync, existsSync, writeFileSync } = require('fs');
const _ = require('lodash');

const cssMap = require('../tmp/css-map.json');

(async () => {
  const selectors = Object.keys(cssMap.nodes);

  const stats = {
    selectorTotal: selectors.length,
    totalLines: 0,
    unusedCountTotal: 0,
    unusedSizeTotal: 0,
    unusedPurgeCssTotal: 0,
    unusedUnCssTotal: 0,
    internalCount: 0,
    internalLines: 0,
    pageSpecificCount: 0,
    pageSpecificLines: 0,
    filesInfo: {},
    baseSelectorInfo: {},
  };

  const unusedNodes = [];
  const unusedPurge = [];
  const unusedUncss = [];

  Object.keys(cssMap.nodes).forEach((rejectedSelector) => {
    const selectorInfo = cssMap.nodes[rejectedSelector];
    selectorInfo.selector = rejectedSelector;
    stats.totalLines += selectorInfo.rulesLength;

    if (selectorInfo.rejectedPurge || selectorInfo.rejectedUncss) {
      if (selectorInfo.internalFile) {
        stats.internalCount++;
        stats.internalLines += selectorInfo.rulesLength;

        if (selectorInfo.file.indexOf('/pages/') > -1) {
          stats.pageSpecificCount++;
          stats.pageSpecificLines += selectorInfo.rulesLength;
        }
      }

      if (selectorInfo.rejectedPurge && selectorInfo.rejectedUncss) {
        stats.unusedCountTotal++;
        stats.unusedSizeTotal += selectorInfo.rulesLength;
        unusedNodes.push(selectorInfo);
      }
      if (selectorInfo.rejectedPurge) {
        stats.unusedPurgeCssTotal++;
        unusedPurge.push(selectorInfo);

        if (stats.filesInfo[selectorInfo.file]) {
          stats.filesInfo[selectorInfo.file].count++;
          stats.filesInfo[selectorInfo.file].size += selectorInfo.rulesLength;
        } else {
          stats.filesInfo[selectorInfo.file] = {
            count: 1,
            size: selectorInfo.rulesLength,
          };
        }

        if (stats.baseSelectorInfo[selectorInfo.baseSelector]) {
          stats.baseSelectorInfo[selectorInfo.baseSelector].count++;
          stats.baseSelectorInfo[selectorInfo.baseSelector].size +=
            selectorInfo.rulesLength;
        } else {
          stats.baseSelectorInfo[selectorInfo.baseSelector] = {
            count: 1,
            size: selectorInfo.rulesLength,
          };
        }
      }
      if (selectorInfo.rejectedUncss) {
        stats.unusedUnCssTotal++;
        unusedUncss.push(selectorInfo);
      }
    }
  });

  writeFileSync('static/unused-stats.json', JSON.stringify(stats, null, 2));

  writeFileSync(
    'static/css-map-all.json',
    JSON.stringify(_.values(cssMap.nodes), null, 2)
  );
  writeFileSync(
    'static/css-map-unused.json',
    JSON.stringify(unusedNodes, null, 2)
  );
  writeFileSync(
    'static/css-map-purge.json',
    JSON.stringify(unusedPurge, null, 2)
  );
  writeFileSync(
    'static/css-map-uncss.json',
    JSON.stringify(unusedUncss, null, 2)
  );
})();
