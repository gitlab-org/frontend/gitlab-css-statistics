const http = require('http');
const https = require('https');
const { mkdirSync, existsSync, createWriteStream } = require('fs');
const puppeteer = require('puppeteer');

const config = require('../crawler_config');

if (!existsSync('tmp')) {
  mkdirSync('tmp');
}

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  const downloadCSS = async (baseUrl, savePath) => {
    try {
      // Increase default timeout from 30 to 60.
      // See https://stackoverflow.com/questions/52163547/node-js-puppeteer-how-to-set-navigation-timeout
      // for more details
      await page.setDefaultNavigationTimeout(60000);

      // Go to the target page.
      console.log(`Opening page ${baseUrl} and saving to path ${savePath}...`);
      await page.goto(baseUrl, { waitUntil: 'networkidle2' });
      await page.waitForTimeout(5000);

      const content = await page.content();

      const cssUrl = await page.evaluate(() => {
        const cssTags = window.document.querySelectorAll(
          'link[rel="stylesheet"]'
        );
        let cssUrl;
        cssTags.forEach((node) => {
          console.log('Found Stylesheet with - ' + node.href);
          if (node.href.indexOf('application-') > -1) {
            console.log('Found CSS Reference: ' + node.href);
            cssUrl = node.href;
          } else if (node.href.indexOf('application_dark-') > -1) {
            console.log('Found CSS Dark Reference: ' + node.href);
            cssUrl = node.href;
          }
        });
        if (!cssUrl) cssUrl = cssTags.length;
        return cssUrl;
      });

      const file = createWriteStream(savePath);
      if (cssUrl) {
        if (cssUrl.indexOf('https://') > -1) {
          https.get(cssUrl, (res) => {
            res.pipe(file);
          });
        } else {
          http.get(cssUrl, (res) => {
            res.pipe(file);
          });
        }

        console.log('Result URL : ' + cssUrl);
      } else {
        console.error('No CSS Url was found for ' + baseUrl);
      }
    } catch (ex) {
      console.error('Problem crawling CSS Base URL: ' + baseUrl + ':', ex);
    }
  };

  await downloadCSS(config.comBaseURL, 'tmp/application-com.css');

  console.log('---------- Downloading local CSS -----------');

  // Lets login locally into GDK
  await page.goto('http://127.0.0.1:3000/users/sign_in');
  await page.waitForTimeout(5000);

  // Login
  await page.type('#user_login', 'root');
  await page.type('#user_password', '5iveL!fe');
  await page.click('.btn.btn-confirm');
  await page.waitForTimeout(5000);

  // Switch to standard
  await page.goto('http://127.0.0.1:3000/-/profile/preferences');
  await page.waitForTimeout(5000);
  await page.click('#user_theme_id_1');
  await page.waitForTimeout(5000);

  await downloadCSS(config.localBaseURL, 'tmp/application-local.css');

  // Switch to dark
  await page.goto('http://127.0.0.1:3000/-/profile/preferences');
  await page.waitForTimeout(5000);
  await page.click('#user_theme_id_11');
  await page.waitForTimeout(5000);

  await downloadCSS(config.localBaseURL, 'tmp/application-dark.css');

  await page.goto('http://127.0.0.1:3000/-/profile/preferences');
  await page.waitForTimeout(5000);
  await page.click('#user_theme_id_1');
  await page.waitForTimeout(5000);

  console.log('Closing browser');
  await browser.close();
})();
