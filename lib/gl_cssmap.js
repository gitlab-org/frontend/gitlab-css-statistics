const postcss = require('postcss');
const { readFileSync, writeFileSync } = require('fs');

const config = require('../crawler_config');

(async () => {
  const localCSSString = readFileSync('static/application-local.css');
  const cssRoot = postcss.parse(localCSSString, {
    from: 'static/application-local.css',
  });

  const nodeMap = { nodes: {} };
  const duplicateSelectors = [];
  const checkNodes = (selectedNodes) => {
    const resultNodes = {};
    for (let i = 0; i < selectedNodes.length; i++) {
      const node = selectedNodes[i];
      if (node.type === 'rule') {
        const newNodeInfo = {
          file: '',
          filePosition: 0,
          mediaQuery: '',
          internalFile: false,
          selectorLength: node.selector.length,
          rulesLength: node.nodes.toString().length,
          rule: node.nodes.toString(),
          baseSelector: node.selector.split(' ')[0].replace(',', ''),
          rejectedPurge: false,
          rejectedUncss: false,
        };

        const prevNode = node.prev();
        if (prevNode && prevNode.type === 'comment') {
          const fileInfos = prevNode.text.split(',');
          if (fileInfos.length === 2) {
            let fileName = fileInfos[1].trim();
            if (
              fileName.indexOf('app/assets') > -1 ||
              fileName.indexOf('/@gitlab/') > -1
            ) {
              newNodeInfo.internalFile = true;
            }
            if (fileName.indexOf('font-awesome-rails') > -1) {
              fileName = 'font-awesome-rails';
            }

            newNodeInfo.file = fileName;
            newNodeInfo.filePosition = fileInfos[0].replace('line ', '');
          }
        }

        let mqFlattened = '';
        if (node.parent.type === 'atrule' && node.parent.name) {
          newNodeInfo.mediaQuery = node.parent.params;
          // mqFlattened - (min-width: 576px)
          mqFlattened = newNodeInfo.mediaQuery
            .replace('(', '')
            .replace(')', '')
            .replace(/:/gi, '_')
            .replace(/ /gi, '_');
        }

        let nodeKey = mqFlattened
          ? mqFlattened + '|' + node.selector
          : node.selector;

        if (nodeMap.nodes[nodeKey]) {
          if (node.parent.type === 'atrule' && node.parent.name) {
            nodeMap.nodes[nodeKey] = newNodeInfo;
          } else if (node.parent.type === 'root') {
            console.log(
              'ALREADY EXISTS ' + node.selector + '/' + newNodeInfo.file
            );
            newNodeInfo.selector = node.selector;
            duplicateSelectors.push(newNodeInfo);
          }
        } else {
          nodeMap.nodes[nodeKey] = newNodeInfo;
        }
      } else if (node.type === 'atrule' && node.nodes) {
        checkNodes(node.nodes);
      }
    }
    return resultNodes;
  };

  checkNodes(cssRoot.nodes);

  // console.log('N ', nodeMap);
  writeFileSync('tmp/css-map.json', JSON.stringify(nodeMap, null, 2));
  writeFileSync(
    'static/duplicate-selectors.json',
    JSON.stringify(duplicateSelectors, null, 2)
  );
})();
