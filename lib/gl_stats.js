const postcss = require('postcss');
const cssstats = require('cssstats');
const { readFileSync, writeFileSync } = require('fs');
const _ = require('lodash');

(async () => {
  const cssMap = require('../static/css-map-all.json');
  const duplicateSelectors = require('../static/duplicate-selectors.json');
  const localCSSString = readFileSync('static/application-local.css');

  const directories = [
    'node_modules/@gitlab/ui/',
    'node_modules/bootstrap/',
    'node_modules/bootstrap-vue/',
    'node_modules/@gitlab/at.js/',
    'node_modules/pikaday/scss/',
    'node_modules/dropzone/',
    'app/assets/stylesheets/framework/',
    'app/assets/stylesheets/pages/',
    'app/assets/stylesheets/components/',
    'app/assets/stylesheets/vendors/',
    'vendor/assets/stylesheets/',
    'app/assets/stylesheets/',
  ];

  const directoryCounts = {};
  directories.forEach((val) => {
    directoryCounts[val] = {
      directory: val,
      selectorCount: 0,
      rulesLength: 0,
    };
  });

  directoryCounts['other'] = {
    directory: 'other',
    selectorCount: 0,
    rulesLength: 0,
  };

  _.values(cssMap).forEach((val) => {
    let foundDir = false;
    _.values(directoryCounts).forEach((dirVal) => {
      if (!foundDir) {
        if (val.file.indexOf(dirVal.directory) > -1) {
          dirVal.selectorCount++;
          dirVal.rulesLength += val.rulesLength;
          foundDir = true;
        }
      }
    });
    if (!foundDir) {
      directoryCounts['other'].selectorCount++;
      directoryCounts['other'].rulesLength += val.rulesLength;
    }
  });

  let fileCounts = _.reduce(
    cssMap,
    (result, val, key) => {
      const el =
        result[val.file] ||
        (result[val.file] = {
          file: val.file,
          count: 0,
          size: 0,
        });

      el.count++;
      el.size += val.rulesLength;
      return result;
    },
    {}
  );

  fileCounts = _.orderBy(fileCounts, ['size'], ['desc']);

  postcss()
    .use(
      cssstats({
        specificityGraph: true,
        sortedSpecificityGraph: true,
        repeatedSelectors: true,
        propertyResets: true,
        vendorPrefixedProperties: true,
      })
    )
    .process(localCSSString)
    .then((result) => {
      const stats = result.messages[0].stats;
      const statsObject = {};
      statsObject.generated = new Date();
      statsObject.size = stats.size;
      statsObject.gzipSize = stats.gzipSize;
      statsObject.humanizedSize = stats.humanizedSize;
      statsObject.humanizedGzipSize = stats.humanizedGzipSize;

      statsObject.directories = _.orderBy(
        directoryCounts,
        ['rulesLength'],
        ['desc']
      );
      statsObject.files = fileCounts;

      statsObject.rulesCount = stats.rules.total;
      statsObject.selectorsCount = stats.selectors.total;

      statsObject.declarationsCount = stats.declarations.total;
      statsObject.declarationsUnique = stats.declarations.unique;

      statsObject.duplicateSelectorsCount = duplicateSelectors.length;

      // console.log(_.keys(stats.declarations.properties));

      statsObject.displayNoneCount = stats.declarations.getPropertyValueCount(
        'display',
        'none'
      );

      statsObject.colorsCount = stats.declarations.properties.color.length;
      statsObject.colorsUnique = stats.declarations.getUniquePropertyCount(
        'color'
      );
      statsObject.colors = _.transform(
        _.countBy(_.sortBy(stats.declarations.properties.color)),
        (res, val, key) => {
          res.push({ color: key, count: val });
        },
        []
      );

      statsObject.backgroundColorsCount =
        stats.declarations.properties['background-color'].length;
      statsObject.backgroundColorsUnique = stats.declarations.getUniquePropertyCount(
        'background-color'
      );
      statsObject.backgroundColors = _.transform(
        _.countBy(_.sortBy(stats.declarations.properties['background-color'])),
        (res, val, key) => {
          res.push({ color: key, count: val });
        },
        []
      );

      statsObject.fontFamiliesCount = stats.declarations.getAllFontFamilies().length;
      statsObject.fontFamiliesUnique = stats.declarations.getUniquePropertyCount(
        'font-family'
      );
      statsObject.fontFamilies = _.transform(
        _.countBy(_.sortBy(stats.declarations.properties['font-family'])),
        (res, val, key) => {
          res.push({ font: key, count: val });
        },
        []
      );

      statsObject.fontSizesCount = stats.declarations.getAllFontSizes().length;
      statsObject.fontSizesUnique = stats.declarations.getUniquePropertyCount(
        'font-size'
      );
      statsObject.fontSizes = _.transform(
        _.countBy(_.sortBy(stats.declarations.properties['font-size'])),
        (res, val, key) => {
          res.push({ fontSize: key, count: val });
        },
        []
      );

      const vendorPrefixesMapped = _.transform(
        stats.declarations.getVendorPrefixed(),
        (res, val, key) => {
          res.push(val.property + ': ' + val.value);
        },
        []
      );

      statsObject.vendorPrefixCount = stats.declarations.getVendorPrefixed().length;
      statsObject.vendorPrefixed = _.transform(
        _.countBy(_.sortBy(vendorPrefixesMapped)),
        (res, val, key) => {
          res.push({ prop: key, count: val });
        },
        []
      );

      statsObject.mediaQueriesCount = stats.mediaQueries.total;
      statsObject.mediaQueriesUnique = stats.mediaQueries.unique;
      statsObject.mediaQueries = _.transform(
        _.countBy(_.sortBy(stats.mediaQueries.values)),
        (res, val, key) => {
          res.push({ query: key, count: val });
        },
        []
      );

      statsObject.biggestSelectors = _.slice(
        _.orderBy(_.values(cssMap), ['selectorLength'], ['desc']),
        0,
        20
      );

      statsObject.biggestRules = _.slice(
        _.orderBy(_.values(cssMap), ['rulesLength'], ['desc']),
        0,
        20
      );

      statsObject.duplicateSelectors = duplicateSelectors;

      writeFileSync(
        'static/css-stats.json',
        JSON.stringify(statsObject, null, 2)
      );

      writeFileSync(
        'static/css-stats-full.json',
        JSON.stringify(stats, null, 2)
      );
    });
  // console.log('N ', nodeMap);
})();
