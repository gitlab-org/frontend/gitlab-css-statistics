# gl-css-statistics

## Generating Startup CSS

### Local GDK setup

Ensure you have a local GDK running which uses all standard defaults
[as described in the GDK installation docs](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#install-dependencies-and-gdk):

- [ ] Cloned to `gitlab-development-kit`, in the same parent directory as this project.
- [ ] User is `root`
- [ ] Root password is `5iveL!fe`
- [ ] Protocol is http (not https)
- [ ] `gdk doctor` shows no errors
- [ ] `gdk update` runs successfully and starts the GDK
- [ ] The apps loads successfully at http://127.0.0.1:3000

### Generate startup css

From this repo:

- [ ] Use the same current node version as the GDK (`asdf install` will do it automatically)
- [ ] `yarn install`
- [ ] `yarn cssdownload` - downloads necessary css files to `tmp` dir
- [ ] `yarn extract-startup` - extracts and saves startup css to `tmp/startup_css`
